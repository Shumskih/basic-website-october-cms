<?php

namespace Shumskih\Resources;

use System\Classes\PluginBase;

class Plugin extends PluginBase
{
    public function pluginDetails() {
        return [
            'name' => 'Shumskih',
            'description' => 'Provides company resources',
            'author' => 'Aleksander Shumskih',
            'icon' => 'icon-leaf'
        ];
    }

    public function registerComponents()
    {
        return [
            '\Shumskih\Resources\Components\Links' => 'resourcesLinks'
        ];
    }
}