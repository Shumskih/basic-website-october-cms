<?php

/* A:\OpenServer\domains\octobercms.loc/plugins/shumskih/resources/components/links/default.htm */
class __TwigTemplate_648d570fdf22571fcaccff87e10719a1c3622446f5881c925839967e5860652a extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<ul class=\"list-group\">
    ";
        // line 2
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["links"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["link"]) {
            // line 3
            echo "        <li class=\"list-group-item\">
            <a href=\"";
            // line 4
            echo twig_escape_filter($this->env, $context["link"], "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $context["link"], "html", null, true);
            echo "</a>
        </li>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['link'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 7
        echo "</ul>";
    }

    public function getTemplateName()
    {
        return "A:\\OpenServer\\domains\\octobercms.loc/plugins/shumskih/resources/components/links/default.htm";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  44 => 7,  33 => 4,  30 => 3,  26 => 2,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<ul class=\"list-group\">
    {% for link in links %}
        <li class=\"list-group-item\">
            <a href=\"{{ link }}\">{{ link }}</a>
        </li>
    {% endfor %}
</ul>", "A:\\OpenServer\\domains\\octobercms.loc/plugins/shumskih/resources/components/links/default.htm", "");
    }
}
