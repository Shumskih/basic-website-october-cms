<?php

/* A:\OpenServer\domains\octobercms.loc/themes/acme/partials/site/footer.htm */
class __TwigTemplate_459a74bf960fc05e66ae41e2721575f6dd33f5bb9509aef77900917b7d0ab583 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<p class=\"text-center\">Copyright &copy; 2018 Acme Services</p>";
    }

    public function getTemplateName()
    {
        return "A:\\OpenServer\\domains\\octobercms.loc/themes/acme/partials/site/footer.htm";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<p class=\"text-center\">Copyright &copy; 2018 Acme Services</p>", "A:\\OpenServer\\domains\\octobercms.loc/themes/acme/partials/site/footer.htm", "");
    }
}
