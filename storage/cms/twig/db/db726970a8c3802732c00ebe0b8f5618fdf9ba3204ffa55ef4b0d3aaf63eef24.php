<?php

/* A:\OpenServer\domains\octobercms.loc/themes/acme/pages/contact.htm */
class __TwigTemplate_0cbf40f651c29b70f64074afbacf96b4c06e7295eb4d79ce123478063f129a09 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<h1>Contact Us</h1>
<form>
  <div class=\"form-group\">
    <label>Name</label>
    <input type=\"text\" class=\"form-control\" placeholder=\"Enter Name\">
  </div>
  <div class=\"form-group\">
    <label>Email</label>
    <input type=\"email\" class=\"form-control\" placeholder=\"Enter Email\">
  </div>
  <div class=\"form-group\">
    <label>Message</label>
    <textarea class=\"form-control\" placeholder=\"Enter Message\"></textarea>
  </div>
  <button type=\"submit\" class=\"btn btn-default\">Submit</button>
</form>";
    }

    public function getTemplateName()
    {
        return "A:\\OpenServer\\domains\\octobercms.loc/themes/acme/pages/contact.htm";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<h1>Contact Us</h1>
<form>
  <div class=\"form-group\">
    <label>Name</label>
    <input type=\"text\" class=\"form-control\" placeholder=\"Enter Name\">
  </div>
  <div class=\"form-group\">
    <label>Email</label>
    <input type=\"email\" class=\"form-control\" placeholder=\"Enter Email\">
  </div>
  <div class=\"form-group\">
    <label>Message</label>
    <textarea class=\"form-control\" placeholder=\"Enter Message\"></textarea>
  </div>
  <button type=\"submit\" class=\"btn btn-default\">Submit</button>
</form>", "A:\\OpenServer\\domains\\octobercms.loc/themes/acme/pages/contact.htm", "");
    }
}
