# A basic website built with October CMS

This is a basic website created using an October CMS which was build on top of Laravel.

##### Tasks that was solve:

* Build a custom theme
* Create a custom plugin

##### Used technologies:

* PHP
* October CMS
* Bootstrap CSS Framework
